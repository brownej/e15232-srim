use e15232_srim::{integrate, Projectile, Target, Thickness};

fn main() {
    let args = std::env::args().skip(1).collect::<Vec<_>>();

    if args.len() != 4 {
        eprintln!("USAGE:");
        eprintln!("calib <BEAM> <BEAM_ENERGY> <JET_AREAL_DENSITY> <IC_PRESSURE>");
        eprintln!("BEAM: '34S', '34Cl', or '34Ar'");
        eprintln!("BEAM_ENERGY: MeV/c^2");
        eprintln!("JET_AREAL_DENSITY: atoms/cm^2");
        eprintln!("IC_PRESSURE: Torr");
        std::process::exit(1);
    }

    let beam = match args[0].as_ref() {
        "34S" => Projectile::S34,
        "34Cl" => Projectile::Cl34,
        "34Ar" => Projectile::Ar34,
        _ => panic!("failed to parse beam"),
    };
    let beam_energy = args[1].parse::<f64>().expect("failed to parse beam_energy");
    let jet_areal_density = args[2]
        .parse::<f64>()
        .expect("failed to parse jet_areal_density");
    let ic_pressure = args[3].parse::<f64>().expect("failed to parse ic_pressure");
    let ic_temperature = 293.15;

    let layers = vec![
        (
            Target::Helium,
            Thickness::ArealDensity {
                areal_density: jet_areal_density,
                depth: 3.0,
            },
        ),
        (Target::Mylar, Thickness::Depth { depth: 3e-3 }),
        (
            Target::Isobutane,
            Thickness::IdealGas {
                pressure: ic_pressure,
                temperature: ic_temperature,
                depth: 20.0,
            },
        ),
        (
            Target::Isobutane,
            Thickness::IdealGas {
                pressure: ic_pressure,
                temperature: ic_temperature,
                depth: 36.6,
            },
        ),
        (
            Target::Isobutane,
            Thickness::IdealGas {
                pressure: ic_pressure,
                temperature: ic_temperature,
                depth: 36.6,
            },
        ),
        (
            Target::Isobutane,
            Thickness::IdealGas {
                pressure: ic_pressure,
                temperature: ic_temperature,
                depth: 73.2,
            },
        ),
        (
            Target::Isobutane,
            Thickness::IdealGas {
                pressure: ic_pressure,
                temperature: ic_temperature,
                depth: 183.0,
            },
        ),
    ];

    let mut de_sum = 0.0;
    for targ in layers.iter() {
        let de = integrate(beam, beam_energy - de_sum, targ.0, targ.1);
        de_sum += de.energy_loss;
        print!("{} ", de.energy_loss * 1e3);
    }

    println!("{}", beam_energy - de_sum);
}
