use e15232_srim::{integrate, Projectile, Target, Thickness};

fn main() {
    let args = std::env::args().skip(1).collect::<Vec<_>>();

    if args.len() != 10 {
        eprintln!("USAGE:");
        eprintln!("calib <BEAM> <BEAM_ENERGY> <JET_DEPTH> <JET_AREAL_DENSITY> <WINDOW_DEPTH> <IC_PRESSURE> <DEAD_LAYER_DEPTH> <IC_X_DEPTH_SCALING> <IC_Y_DEPTH_SCALING> <IC_DE_DEPTH_SCALING>");
        eprintln!("BEAM: '34S', '34Cl', or '34Ar'");
        eprintln!("BEAM_ENERGY: MeV/c^2");
        eprintln!("JET_DEPTH: mm");
        eprintln!("JET_AREAL_DENSITY: atoms/cm^2");
        eprintln!("WINDOW_DEPTH: mm");
        eprintln!("IC_PRESSURE: Torr");
        eprintln!("DEAD_LAYER_DEPTH: mm");
        std::process::exit(1);
    }

    let beam = match args[0].as_ref() {
        "34S" => Projectile::S34,
        "34Cl" => Projectile::Cl34,
        "34Ar" => Projectile::Ar34,
        _ => panic!("failed to parse beam"),
    };
    let beam_energy = args[1].parse::<f64>().expect("failed to parse beam_energy");
    let jet_depth = args[2].parse::<f64>().expect("failed to parse jet_depth");
    let jet_areal_density = args[3]
        .parse::<f64>()
        .expect("failed to parse jet_areal_density");
    let window_depth = args[4].parse::<f64>().expect("failed to parse window_depth");
    let ic_pressure = args[5].parse::<f64>().expect("failed to parse ic_pressure");
    let dead_layer_depth = args[6].parse::<f64>().expect("failed to parse dead_layer_depth");
    let ic_x_depth = args[7].parse::<f64>().expect("failed to parse dead_layer_depth");
    let ic_y_depth = args[8].parse::<f64>().expect("failed to parse dead_layer_depth");
    let ic_de_depth = args[9].parse::<f64>().expect("failed to parse dead_layer_depth");
    let ic_temperature = 293.15;

    let layers = vec![
        (
            Target::Helium,
            Thickness::ArealDensity {
                areal_density: jet_areal_density,
                depth: jet_depth,
            },
        ),
        (Target::Mylar, Thickness::Depth { depth: window_depth }),
        (
            Target::Isobutane,
            Thickness::IdealGas {
                pressure: ic_pressure,
                temperature: ic_temperature,
                depth: dead_layer_depth,
            },
        ),
        (
            Target::Isobutane,
            Thickness::IdealGas {
                pressure: ic_pressure,
                temperature: ic_temperature,
                depth: ic_x_depth * 36.6,
            },
        ),
        (
            Target::Isobutane,
            Thickness::IdealGas {
                pressure: ic_pressure,
                temperature: ic_temperature,
                depth: ic_y_depth * 36.6,
            },
        ),
        (
            Target::Isobutane,
            Thickness::IdealGas {
                pressure: ic_pressure,
                temperature: ic_temperature,
                depth: ic_de_depth * 73.2,
            },
        ),
        (
            Target::Isobutane,
            Thickness::IdealGas {
                pressure: ic_pressure,
                temperature: ic_temperature,
                depth: 183.0,
            },
        ),
    ];

    let mut de_sum = 0.0;
    for targ in layers.iter() {
        let de = integrate(beam, beam_energy - de_sum, targ.0, targ.1);
        de_sum += de.energy_loss;
        print!("{} ", de.energy_loss * 1e3);
    }

    println!("{}", beam_energy - de_sum);
}
