use lazy_static::lazy_static;
use splines::{Interpolation, Key, Spline};
use std::collections::HashMap;

pub use splines;

/// Boltzmann constant (k_B) \[MeV/K]
pub const BOLTZMANN: f64 = 8.617333262145e-11;

lazy_static! {
    pub static ref SRIM_DATA: HashMap<(Projectile, Target), SrimData> = {
        use Projectile::*;
        use Target::*;

        [
            (S34, Helium, include_str!("data/34S_helium.txt")),
            (Cl34, Helium, include_str!("data/34Cl_helium.txt")),
            (Ar34, Helium, include_str!("data/34Ar_helium.txt")),
            (Cl37, Helium, include_str!("data/37Cl_helium.txt")),
            (Ar37, Helium, include_str!("data/37Ar_helium.txt")),
            (K37, Helium, include_str!("data/37K_helium.txt")),
            (S34, Aluminum, include_str!("data/34S_aluminum.txt")),
            (Cl34, Aluminum, include_str!("data/34Cl_aluminum.txt")),
            (Ar34, Aluminum, include_str!("data/34Ar_aluminum.txt")),
            (Cl37, Aluminum, include_str!("data/37Cl_aluminum.txt")),
            (Ar37, Aluminum, include_str!("data/37Ar_aluminum.txt")),
            (K37, Aluminum, include_str!("data/37K_aluminum.txt")),
            (S34, Mylar, include_str!("data/34S_mylar.txt")),
            (Cl34, Mylar, include_str!("data/34Cl_mylar.txt")),
            (Ar34, Mylar, include_str!("data/34Ar_mylar.txt")),
            (Cl37, Mylar, include_str!("data/37Cl_mylar.txt")),
            (Ar37, Mylar, include_str!("data/37Ar_mylar.txt")),
            (K37, Mylar, include_str!("data/37K_mylar.txt")),
            (S34, Isobutane, include_str!("data/34S_isobutane.txt")),
            (Cl34, Isobutane, include_str!("data/34Cl_isobutane.txt")),
            (Ar34, Isobutane, include_str!("data/34Ar_isobutane.txt")),
            (Cl37, Isobutane, include_str!("data/37Cl_isobutane.txt")),
            (Ar37, Isobutane, include_str!("data/37Ar_isobutane.txt")),
            (K37, Isobutane, include_str!("data/37K_isobutane.txt")),
        ]
        .iter()
        .map(|(proj, targ, v)| ((*proj, *targ), parse_srim_splines(v)))
        .collect()
    };
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Projectile {
    S34,
    Cl34,
    Ar34,
    Cl37,
    Ar37,
    K37,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Target {
    Helium,
    Aluminum,
    Mylar,
    Isobutane,
}

impl Target {
    /// The number of atoms in each molecule. If this material is not a molecule, it should be 1.0.
    pub fn atoms(&self) -> f64 {
        match self {
            Self::Helium => 1.0,
            Self::Aluminum => 1.0,
            Self::Mylar => 22.0,
            Self::Isobutane => 14.0,
        }
    }
}

#[derive(Debug)]
pub struct SrimData {
    /// Density of the target \[atoms/cm^3]
    pub density: f64,
    /// Electronic stopping power \[MeV / mm] vs projectile energy \[MeV]
    pub stopping_power_elec: Spline<f64, f64>,
    /// Nuclear stopping power \[MeV / mm] vs projectile energy \[MeV]
    pub stopping_power_nucl: Spline<f64, f64>,
    /// Projected range \[mm] vs projectile energy \[MeV]
    pub range_energy: Spline<f64, f64>,
    /// Projectile energy \[MeV] vs projected range \[mm]
    pub energy_range: Spline<f64, f64>,
    /// Longitudinal straggling \[mm] vs projectile energy \[MeV]
    pub straggling_long: Spline<f64, f64>,
    /// Lateral straggling \[mm] vs projectile energy \[MeV]
    pub straggling_lat: Spline<f64, f64>,
}

fn unit_energy(unit: &str) -> Option<f64> {
    match unit {
        "keV" => Some(1e-3),
        "MeV" => Some(1e0),
        _ => None,
    }
}

fn unit_length(unit: &str) -> Option<f64> {
    match unit {
        "A" => Some(1e-7),
        "um" => Some(1e-3),
        "mm" => Some(1e0),
        _ => None,
    }
}

fn parse_srim_splines(file: &str) -> SrimData {
    let density = file
        .lines()
        .nth(9)
        .unwrap()
        .split_whitespace()
        .nth(6)
        .unwrap()
        .parse::<f64>()
        .unwrap();

    let mut spline_vecs = (
        Vec::new(),
        Vec::new(),
        Vec::new(),
        Vec::new(),
        Vec::new(),
        Vec::new(),
    );
    // the number of lines at the start varies based on the target.
    // we skip until we reach a marker line and then skip the known number of lines until
    // we get to the data
    file.lines()
        .skip_while(|line| line != &" ====================================")
        .skip(8)
        .take(105)
        .for_each(|line| {
            let fields = line.split_whitespace().collect::<Vec<_>>();
            let energy = fields[0].parse::<f64>().unwrap() * unit_energy(fields[1]).unwrap();
            let stopping_power_elec = fields[2].parse::<f64>().unwrap();
            let stopping_power_nucl = fields[3].parse::<f64>().unwrap();
            let range = fields[4].parse::<f64>().unwrap() * unit_length(fields[5]).unwrap();
            let straggling_long =
                fields[6].parse::<f64>().unwrap() * unit_length(fields[7]).unwrap();
            let straggling_lat =
                fields[8].parse::<f64>().unwrap() * unit_length(fields[9]).unwrap();

            spline_vecs
                .0
                .push(Key::new(energy, stopping_power_elec, Interpolation::Linear));
            spline_vecs
                .1
                .push(Key::new(energy, stopping_power_nucl, Interpolation::Linear));
            spline_vecs
                .2
                .push(Key::new(energy, range, Interpolation::Linear));
            spline_vecs
                .3
                .push(Key::new(range, energy, Interpolation::Linear));
            spline_vecs
                .4
                .push(Key::new(energy, straggling_long, Interpolation::Linear));
            spline_vecs
                .5
                .push(Key::new(energy, straggling_lat, Interpolation::Linear));
        });

    SrimData {
        density,
        stopping_power_elec: Spline::from_vec(spline_vecs.0),
        stopping_power_nucl: Spline::from_vec(spline_vecs.1),
        range_energy: Spline::from_vec(spline_vecs.2),
        energy_range: Spline::from_vec(spline_vecs.3),
        straggling_long: Spline::from_vec(spline_vecs.4),
        straggling_lat: Spline::from_vec(spline_vecs.5),
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Thickness {
    Depth {
        /// depth of the target \[mm]
        depth: f64,
    },
    ArealDensity {
        /// Areal density (rho_a) \[atoms/cm^2]
        areal_density: f64,
        /// depth of the target \[mm]
        depth: f64,
    },
    /// A cell uniformly filled with an ideal gas
    IdealGas {
        /// pressure of the gas \[Torr]
        pressure: f64,
        /// temperature of the gas \[K]
        temperature: f64,
        /// depth of the target \[mm]
        depth: f64,
    },
}

impl Thickness {
    pub fn depth(&mut self) -> &mut f64 {
        match self {
            Thickness::Depth { depth }
            | Thickness::ArealDensity { depth, .. }
            | Thickness::IdealGas { depth, .. } => depth,
        }
    }
}

pub struct SrimResult {
    /// Energy loss \[MeV] of a projectile (proj) in a target (targ) of a given thickness.
    pub energy_loss: f64,
}

/// Calculate the properties of a projectile (proj) with an initial energy e_init \[MeV] in a target (targ) of a given thickness.
pub fn integrate(proj: Projectile, e_init: f64, targ: Target, thickness: Thickness) -> SrimResult {
    let srim_data = &SRIM_DATA[&(proj, targ)];

    let (depth, density) = match thickness {
        Thickness::Depth { depth } => (depth, 1.0),
        Thickness::ArealDensity {
            areal_density,
            depth,
        } => {
            let density = areal_density / (depth * 1e-1);
            (depth, density)
        }
        Thickness::IdealGas {
            pressure,
            temperature,
            depth,
        } => {
            let density = pressure / (BOLTZMANN * temperature) * 8.321e8 * targ.atoms();
            (depth, density)
        }
    };

    // Knoll, 4th edition pg 37
    // to get dE:
    // 1. R1 = R(E1)
    // 2. R1 - R0 = depth
    // 3. R0 = E(R0)
    // 4. dE = E1 - E0
    //
    // Knoll, 4th edition pg 40
    // To correct for density difference from tables:
    // R1/R0 proportional to rho0/rho1

    // Check if it stops in the material first
    if srim_data.density / density
        * srim_data
            .range_energy
            .clamped_sample(e_init)
            .expect("failed to sample range_energy")
            //.unwrap_or(0.0) // Assume if there's no range data, it's because it's 0
        < depth
    {
        return SrimResult {
            energy_loss: e_init,
        };
    }

    let r1 = srim_data.density / density
        * srim_data
            .range_energy
            .clamped_sample(e_init)
            .expect("failed to sample for r1");
    let r0 = r1 - depth;
    let e0 = srim_data
        .energy_range
        .clamped_sample(r0 * density / srim_data.density)
        .expect("failed to sample for e0");
    //.unwrap_or(0.0); // Assume if there's no range data, it's because it's 0
    let de = e_init - e0;

    SrimResult {
        energy_loss: f64::min(e_init, de),
    }
}
